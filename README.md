# Spring and AWS 3

### Steps

1. Create account in AWS
2. Configure a bucket on S3
3. Generate Access-Key and Secret-Key
4. Copy **_aws-credentials.yml.example_** to **aws-credentials.yml** and replace all values