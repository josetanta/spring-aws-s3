package com.nttdata.samplespring01;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SampleSpring01Application {

	public static void main(String[] args) {
		SpringApplication.run(SampleSpring01Application.class, args);
	}
}
