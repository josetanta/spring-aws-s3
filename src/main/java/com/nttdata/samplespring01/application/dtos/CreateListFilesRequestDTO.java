package com.nttdata.samplespring01.application.dtos;

public record CreateListFilesRequestDTO(String[] entities) {
	
}
