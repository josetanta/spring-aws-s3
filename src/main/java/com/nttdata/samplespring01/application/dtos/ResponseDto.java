package com.nttdata.samplespring01.application.dtos;

public record ResponseDto(
	Integer code,
	String message,
	Object body
) {
	
}
