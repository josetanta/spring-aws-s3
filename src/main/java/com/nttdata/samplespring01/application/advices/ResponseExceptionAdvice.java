package com.nttdata.samplespring01.application.advices;

import com.nttdata.samplespring01.infraestructure.utils.GenericResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MissingRequestHeaderException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
@Slf4j
public class ResponseExceptionAdvice {

	@ExceptionHandler({
		IllegalArgumentException.class,
		MissingRequestHeaderException.class
	})
	public ResponseEntity<?> handleException(Exception ex) {
		log.error("Error {}", ex.getMessage());
		return ResponseEntity.badRequest().body(
			GenericResponse.error(HttpStatus.BAD_REQUEST.name(), ex.getMessage())
		);
	}
}
