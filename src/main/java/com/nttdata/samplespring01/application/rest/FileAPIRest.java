package com.nttdata.samplespring01.application.rest;

import com.itextpdf.text.DocumentException;
import com.nttdata.samplespring01.application.dtos.CreateListFilesRequestDTO;
import com.nttdata.samplespring01.domain.services.FileService;
import com.nttdata.samplespring01.infraestructure.utils.GenericResponse;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

@CrossOrigin(origins = "${spring.cors.origins}")
@RestController
@RequestMapping("/storage")
@RequiredArgsConstructor
public class FileAPIRest {

	private static final Logger log = LoggerFactory.getLogger(FileAPIRest.class);
	private final FileService fileService;

	@CrossOrigin(allowedHeaders = {"X-Send-File-Accepted"}, exposedHeaders = {"X-Send-File-Accepted"})
	@PostMapping("upload")
	public ResponseEntity<?> uploadFile(
		@RequestHeader(value = "X-Send-File-Accepted") Boolean acceptFile,
		@RequestParam(value = "re-name", required = false) String rename,
		@RequestParam MultipartFile file
	) throws IOException {
		boolean ok = Optional.ofNullable(acceptFile)
			.orElseThrow(() -> new IllegalArgumentException("Does not exist contract"));

		if (!ok)
			return ResponseEntity.noContent().build();

		Optional<String> nameFile = fileService.uploadFile(file, rename);

		if (nameFile.isPresent())
			return ResponseEntity.ok(GenericResponse.success("New name of file", nameFile.get()));
		else
			return ResponseEntity.badRequest().body(GenericResponse.error("Error upload file"));
	}

	@CrossOrigin(allowedHeaders = {"X-Allow-Download", "X-Type-File"})
	@GetMapping(value = "download", produces = {
		MediaType.APPLICATION_OCTET_STREAM_VALUE,
		MediaType.APPLICATION_PDF_VALUE,
		MediaType.TEXT_PLAIN_VALUE
	})
	public ResponseEntity<?> downloadFile(
		@RequestParam(name = "filename", required = false) String nameFile,
		@RequestHeader("X-Allow-Download") Boolean allowDownload,
		@RequestHeader("X-Type-File") String typeFile
	) throws IOException, URISyntaxException {
		boolean ok = Optional.ofNullable(allowDownload)
			.orElseThrow(() -> new IllegalArgumentException("Does not exit header"));

		if (!ok)
			throw new IllegalArgumentException("Not permit download file");

		ByteArrayResource resource = fileService.downloadFile(nameFile, typeFile);

		return ResponseEntity.ok(resource);
	}

	@CrossOrigin(
		allowedHeaders = {
			"X-Allow-Read",
			HttpHeaders.CONTENT_TYPE
		},
		exposedHeaders = {"Content-Disposition"}
	)
	@PostMapping(value = "list-entities",
		produces = {
			MediaType.APPLICATION_JSON_VALUE
		},
		consumes = {
			MediaType.APPLICATION_JSON_VALUE
		}
	)
	public ResponseEntity<?> writeTextOnList(
		@RequestHeader("X-Allow-Read") Boolean allowRead,
		@RequestBody CreateListFilesRequestDTO createDto
	) throws IOException {
		boolean allowReadOpt = Optional.ofNullable(allowRead)
			.orElseThrow(() -> new IllegalArgumentException("Require a value for allow read"));

		if (!allowReadOpt)
			throw new IllegalArgumentException("Not permit download file");

		try {
			fileService.writeTextOnList(List.of(createDto.entities()));
			return ResponseEntity.noContent().build();
		} catch (FileNotFoundException | DocumentException e) {
			log.info("Error {}", e.getMessage());
			return ResponseEntity.badRequest()
				.body(GenericResponse.error("Failed write text"));
		}
	}

	@GetMapping("list-files")
	public ResponseEntity<?> getAllFiles() throws IOException {
		List<String> nameFiles = fileService.listFiles();
		return ResponseEntity.ok(GenericResponse.success("List of files", nameFiles));
	}

	@GetMapping("url-file-presigned")
	public ResponseEntity<?> getPresignedURL(@RequestParam String filename) {
		return ResponseEntity.ok(GenericResponse.success("OK", fileService.presignedURL(filename)));
	}

}
