package com.nttdata.samplespring01.infraestructure.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import software.amazon.awssdk.auth.credentials.AwsBasicCredentials;
import software.amazon.awssdk.auth.credentials.AwsCredentials;
import software.amazon.awssdk.auth.credentials.StaticCredentialsProvider;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.s3.S3Client;
import software.amazon.awssdk.services.s3.presigner.S3Presigner;

import java.util.function.Supplier;

@Configuration
@PropertySource(value = "classpath:aws-credentials.yml", factory = YamlPropertySourceFactory.class)
public class AWSConfig {

	@Value("${aws.s3.access-key}")
	private String accessKey;

	@Value("${aws.s3.secret-key}")
	private String secretKey;

	@Value("${aws.s3.region}")
	private String region;

	private final Supplier<AwsCredentials> credentials = () -> AwsBasicCredentials.create(accessKey, secretKey);

	@Bean
	S3Client s3ClientBucket() {
		return S3Client.builder()
			.credentialsProvider(StaticCredentialsProvider.create(credentials.get()))
			.region(Region.of(region))
			.build();
	}

	@Bean
	S3Presigner s3Presigner() {
		return S3Presigner.builder()
			.credentialsProvider(StaticCredentialsProvider.create(credentials.get()))
			.region(Region.of(region))
			.build();
	}
}
