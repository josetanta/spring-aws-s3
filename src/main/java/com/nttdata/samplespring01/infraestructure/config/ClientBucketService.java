package com.nttdata.samplespring01.infraestructure.config;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;
import software.amazon.awssdk.core.ResponseBytes;
import software.amazon.awssdk.core.sync.ResponseTransformer;
import software.amazon.awssdk.services.s3.S3Client;
import software.amazon.awssdk.services.s3.model.*;
import software.amazon.awssdk.services.s3.presigner.S3Presigner;
import software.amazon.awssdk.services.s3.presigner.model.GetObjectPresignRequest;
import software.amazon.awssdk.services.s3.presigner.model.PresignedGetObjectRequest;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Path;
import java.time.Duration;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Slf4j
@Component
@RequiredArgsConstructor
public class ClientBucketService {

	private final S3Client client;
	private final S3Presigner presigner;

	@Value("${aws.s3.bucket}")
	protected String bucketName;

	private static final String FOLDER_DOCUMENT = "documents/";

	public void uploadFile(File file, String key) {
		PutObjectRequest request = PutObjectRequest.builder()
			.bucket(bucketName)
			.key(FOLDER_DOCUMENT + key)
			.build();

		client.putObject(request, Path.of(file.toURI()));
	}

	public Optional<String> uploadFile(MultipartFile multipartFile, String key)
		throws IOException {
		var file = multipartToFile(multipartFile);

		PutObjectRequest request = PutObjectRequest.builder()
			.bucket(bucketName)
			.key(FOLDER_DOCUMENT + key)
			.build();

		var res = client.putObject(request, Path.of(file.toURI()));

		return Optional.of(res.eTag());
	}

	public String getPresignedObject(String keyName) {
		GetObjectRequest request = GetObjectRequest.builder()
			.bucket(bucketName)
			.key(FOLDER_DOCUMENT + keyName)
			.build();

		GetObjectPresignRequest requestPresign = GetObjectPresignRequest.builder()
			.signatureDuration(Duration.ofSeconds(30))
			.getObjectRequest(request)
			.build();

		PresignedGetObjectRequest presignedGetObjectRequest = presigner.presignGetObject(requestPresign);

		return presignedGetObjectRequest.url().toString();
	}

	public ByteArrayResource downloadFile(String key) {
		try {
			log.info("Downloading file {}", key);
			GetObjectRequest request = GetObjectRequest.builder()
				.bucket(bucketName)
				.key(FOLDER_DOCUMENT + key)
				.build();

			ResponseBytes<GetObjectResponse> objectBytes = client.getObject(request, ResponseTransformer.toBytes());
			return new ByteArrayResource(objectBytes.asByteArray());
		} catch (S3Exception ex) {
			log.error(ex.getMessage());
			return null;
		}
	}

	public Set<String> getAllFiles() {
		ListObjectsRequest request = ListObjectsRequest.builder()
			.bucket(bucketName)
			.prefix(FOLDER_DOCUMENT)
			.build();
		ListObjectsResponse listObjects = client.listObjects(request);

		return listObjects.contents().stream()
			.map(object -> object.key().substring(FOLDER_DOCUMENT.length()))
			.filter(key -> !key.isBlank())
			.collect(Collectors.toSet());
	}

	private File multipartToFile(MultipartFile multipart) throws IOException {
		File convertedFile = new File(Objects.requireNonNull(multipart.getOriginalFilename()));
		try (OutputStream fos = new FileOutputStream(convertedFile)) {
			fos.write(multipart.getBytes());
		}
		return convertedFile;
	}
}
