package com.nttdata.samplespring01.infraestructure.utils;

public record ResponseSuccess<T>(
	String message,
	int status,
	boolean success,
	T data
) {

}
