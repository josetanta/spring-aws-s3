package com.nttdata.samplespring01.infraestructure.utils;

public record ResponseError<T>(
	String message,
	int status,
	boolean error,
	T details
) {
	
}
