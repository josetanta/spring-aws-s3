package com.nttdata.samplespring01.infraestructure.utils;

import org.springframework.http.HttpStatus;

public final class GenericResponse {

	private GenericResponse() {
	}

	public static <T> ResponseSuccess<T> empty() {
		return new ResponseSuccess<>(
			HttpStatus.NO_CONTENT.name(),
			HttpStatus.NO_CONTENT.value(),
			true,
			null);
	}

	public static <T> ResponseSuccess<T> success(HttpStatus status, String message, T data) {
		return new ResponseSuccess<>(
			message,
			status.value(),
			true,
			data);
	}

	public static <T> ResponseSuccess<T> success(String message, T data) {
		return new ResponseSuccess<>(
			message,
			HttpStatus.OK.value(),
			true,
			data);
	}

	public static <T> ResponseSuccess<T> success(T data) {
		return new ResponseSuccess<>(
			HttpStatus.OK.name(),
			HttpStatus.OK.value(),
			true,
			data);
	}

	public static <T> ResponseError<T> error(HttpStatus status, String message, T data) {
		return new ResponseError<>(
			message,
			status.value(),
			true,
			data);
	}

	public static <T> ResponseError<T> error(String message, T data) {
		return new ResponseError<>(
			message,
			HttpStatus.BAD_REQUEST.value(),
			true,
			data);
	}

	public static <T> ResponseError<T> error(T data) {
		return new ResponseError<>(
			HttpStatus.BAD_REQUEST.name(),
			HttpStatus.BAD_REQUEST.value(),
			true,
			data);
	}

}
