package com.nttdata.samplespring01.domain.services;

import com.itextpdf.text.DocumentException;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

public interface FileService {
	/**
	 * Upload file to AWS S3
	 *
	 * @param file   request file
	 * @param rename new name
	 * @return String
	 * @throws IOException
	 */
	Optional<String> uploadFile(MultipartFile file, String rename) throws IOException;

	ByteArrayResource downloadFile(String nameFile, String typeFile) throws IOException, URISyntaxException;

	void writeTextOnList(List<String> entities) throws IOException, DocumentException;

	List<String> listFiles() throws IOException;

	String presignedURL(String filename);
}
