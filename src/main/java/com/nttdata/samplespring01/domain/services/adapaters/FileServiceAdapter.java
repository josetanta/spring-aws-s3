package com.nttdata.samplespring01.domain.services.adapaters;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfWriter;
import com.nttdata.samplespring01.domain.services.FileService;
import com.nttdata.samplespring01.infraestructure.config.ClientBucketService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Slf4j
@Service
@RequiredArgsConstructor
public class FileServiceAdapter implements FileService {

	private final ClientBucketService bucketService;

	@Override
	public List<String> listFiles() {

		log.info("list files {}", bucketService.getAllFiles());

		return bucketService.getAllFiles().stream().toList();
	}

	@Override
	public Optional<String> uploadFile(MultipartFile multipart, String rename) throws IOException {
		if (multipart.isEmpty())
			throw new IllegalArgumentException("Can't create this file");
		var isEmpty = rename.isEmpty();
		Matcher matcher = isValidatedFilename(multipart);
		if (!matcher.matches())
			throw new IllegalArgumentException("This file is invalid");

		rename = rename + matcher.group(2);

		return bucketService.uploadFile(multipart,
			isEmpty ? multipart.getOriginalFilename() : rename);
	}

	private static Matcher isValidatedFilename(MultipartFile multipart) {
		Pattern patternFile = Pattern.compile("([^\\s]+(\\.(?i)(pdf|txt|doc|docx)$))");
		return patternFile
			.matcher(Objects.requireNonNull(multipart.getOriginalFilename()));
	}

	@Override
	public ByteArrayResource downloadFile(String nameFile, String typeFile) {
		return bucketService.downloadFile(nameFile);
	}

	@Override
	public void writeTextOnList(List<String> entities)
		throws IOException, DocumentException {
		var filename = "entities-" + System.currentTimeMillis() + ".pdf";

		buildDocumentPdf(entities, filename);

		Path findFile = Paths.get(filename);
		bucketService.uploadFile(findFile.toFile(), filename);
		log.info("Upload a new file {}", filename);

		try {
			log.info("Deleting file with name {} in server", findFile.getFileName());
			Files.delete(findFile);
		} catch (IOException e) {
			log.error(e.getMessage());
		}
	}

	private static void buildDocumentPdf(List<String> entities, String filename) throws DocumentException, IOException {
		Document document = new Document();
		FileOutputStream outputStream = new FileOutputStream(filename);
		PdfWriter.getInstance(document, outputStream);
		document.open();
		Font font = FontFactory.getFont("Monospaced", 12, BaseColor.BLACK);
		document.addTitle("Entities for testing");
		document.addAuthor("josetanta");

		entities.forEach(text -> {
			try {
				Paragraph paragraph = new Paragraph(text, font);
				document.add(paragraph);
			} catch (DocumentException e) {
				log.warn("The document cannot add new paragraph");
			}
		});

		document.close();
		outputStream.close();
	}

	@Override
	public String presignedURL(String filename) {
		return bucketService.getPresignedObject(filename);
	}

}
